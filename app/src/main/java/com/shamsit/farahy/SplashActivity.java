package com.shamsit.farahy;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;



/**
 * Created by Amer Elsayed on 11/21/2015.
 * AlphaMaple.com
 */
public class SplashActivity extends Activity {

    Runnable run;
    Intent intent;
    Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);
        run = new Runnable() {
            @Override
            public void run() {
                intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        handler.postDelayed(run, 3000);

    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        handler.removeCallbacks(run);
        super.onDestroy();
    }
}
